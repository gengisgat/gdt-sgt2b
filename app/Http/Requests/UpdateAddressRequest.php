<?php

namespace App\Http\Requests;

use App\Models\Address;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateAddressRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('address_edit');
    }

    public function rules()
    {
        return [
            'cap' => [
                'string',
                'min:3',
                'required',
                'unique:addresses,cap,' . request()->route('address')->id,
            ],
            'name' => [
                'string',
                'min:3',
                'required',
            ],
        ];
    }
}
